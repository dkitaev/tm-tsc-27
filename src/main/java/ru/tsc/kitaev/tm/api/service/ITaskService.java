package ru.tsc.kitaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IOwnerService<Task> {

    void create(@Nullable String userId, @Nullable String name);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Task findByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Task removeByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @NotNull String description);

    @NotNull
    Task updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @NotNull String description);

    @Nullable
    Task startById(@Nullable String userId, @Nullable String id);

    @NotNull
    Task startByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task startByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Task finishById(@Nullable String userId, @Nullable String id);

    @NotNull
    Task finishByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task finishByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Task changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Task changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Task changeStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status);

}
