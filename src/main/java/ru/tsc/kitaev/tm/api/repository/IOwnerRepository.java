package ru.tsc.kitaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository <E extends AbstractOwnerEntity> extends IRepository<E> {

    @Nullable
    E add(@NotNull String userId, @NotNull E entity);

    void remove(@NotNull String userId, @NotNull E entity);

    void clear(@NotNull String userId);

    @NotNull
    List<E> findAll(@NotNull String userId);

    @NotNull
    List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator);

    @Nullable
    E findById(@NotNull String userId, @NotNull String id);

    @NotNull
    E findByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    E removeById(@NotNull String userId, @NotNull String id);

    @Nullable
    E removeByIndex(@NotNull String userId, @NotNull Integer index);

    boolean existsById(@NotNull String userId, @NotNull String id);

    boolean existsByIndex(@NotNull String userId, int index);

    @NotNull
    Integer getSize(@NotNull String userId);

}
