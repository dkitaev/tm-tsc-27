package ru.tsc.kitaev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.command.AbstractCommand;
import ru.tsc.kitaev.tm.dto.Domain;
import ru.tsc.kitaev.tm.exception.entity.EntityNotFoundException;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected static final String FILE_BINARY = "./data.bin";

    @NotNull
    protected static final String FILE_BASE64 = "./data.base64";

    @NotNull
    @SneakyThrows
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setUsers(serviceLocator.getUserService().findAll());
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        return domain;
    }

    public void setDomain(@Nullable Domain domain) {
        if (domain == null) throw new EntityNotFoundException();
        serviceLocator.getTaskService().clear();
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getProjectService().clear();
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
        serviceLocator.getAuthService().logout();
    }

}
