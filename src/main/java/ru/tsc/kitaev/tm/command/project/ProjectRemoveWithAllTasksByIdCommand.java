package ru.tsc.kitaev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.command.AbstractProjectCommand;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.util.TerminalUtil;

public final class ProjectRemoveWithAllTasksByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-remove-with-tasks-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project with tasks by id...";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter project id");
        @NotNull final String projectId = TerminalUtil.nextLine();
        if (serviceLocator.getProjectService().findById(userId, projectId) == null) throw new ProjectNotFoundException();
        @Nullable final Project taskUpdated = serviceLocator.getProjectTaskService().removeById(userId, projectId);
        if (taskUpdated == null) throw new TaskNotFoundException();
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
